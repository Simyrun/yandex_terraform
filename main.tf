terraform {
 required_providers {
  yandex = {
  source  = "yandex-cloud/yandex"
  version = "0.87.0"
  }
 }
}

resource "yandex_compute_instance" "vm-1" {
    name = "chapter5-lesson2-gruzdev"

    resources {
        cores  = 2
        memory = 2
    }

    boot_disk {
        initialize_params {
            image_id = "fd80qm01ah03dkqb14lc"
        }
    }

    network_interface {
        subnet_id = "e9b8ilpp3vu2398rtrpl"
        nat       = true
    }

    metadata = {
        user-data = "${file("user_data")}"
}
/*
    metadata = {
        ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    } */
}

output "ip_address" {
    value = yandex_compute_instance.vm-1.network_interface.0.ip_address
}
output "ip_addres_pub" {
    value = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}
