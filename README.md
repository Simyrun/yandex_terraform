1. Установить Yandex Cloud console:

    ``` curl -sSL https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash ```

2.  перейти по ссылке и получить Oauth

  

3. ввести в консоле (для работы требуется браузер):

	    3.1 выполнить `yc init`

	    3.2 ввести токен федерации   ```yc init --federation-id=bpfpfctkh7focc85u9sq```

	    3.4 сгенерировать токtн  ``` yc iam create-token ```

  

4. Вставить сгенерированный ранее токен в переменную `yandex-token`

	    4.1 `terraform init`

	    4.2 `terraform apply`

Terraform подключится к Yandex Cloud  и создаст ВМ с преднастроенными учётными записсями и ключами доступа SSH
